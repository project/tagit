<?php
/**
* Define this Export UI plugin.
*/
$plugin = array(
  'schema' => 'tagit_implementations',  // As defined in hook_schema().
  'access' => 'administer site configuration',  // Define a permission users must have to access these pages.
  // Define the menu item.
  'menu' => array(
    'menu item' => 'tagit',
    'menu title' => 'Tag-it',
    'menu description' => 'Configuration for Tag-it plugin.',
  ),
  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Tagit preset'),
  'title plural proper' => t('Tagit presets'),
  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'tagit_implementation_settings_form',
    'submit' => 'tagit_implementation_settings_form_submit'
  ),
  'handler' => array(
    'class' => 'tagit_export_ui',
    'parent' => 'ctools_export_ui',
  ),
);