Tagit is a jquery plugin supplied by aehlke

Documentation can be found at https://github.com/aehlke/tag-it/blob/master/README.markdown

This module will supply 'chosen' style text strings to a text field.  Contrast this with the chosen module that provides the same for a select field.

PRE-INSTALLATION

Download the tag-it jquery plugin from http://aehlke.github.io/tag-it/
Copy the contents of the plugin to the libraries folder making sure the folder name is 'tagit'
Create a text file called VERSION with no suffix and enter the version number into the file so that is all it contains and put the file into the tagit library folder.

INSTALLATION

Install as normal module

CONFIGURATION

Go to settings under configuration >> user interface
Only use for textfields.  For select fields use the chosen module

USAGE

This module currently supports multiple tagit implementations per page
It is better suited to custom autocomplete implementations, since there are few available in Drupal by default