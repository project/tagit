<?php

/**
 * @file
 * tagit administration pages.
 */


/**
 * Menu callback/form-builder for the form to create or edit a search page.
 */
function tagit_implementation_settings_form($form, &$form_state, $implementation = NULL) {
  // Initializes form with common settings.
  $form['implementation_id'] = array(
      '#type' => 'value',
      '#value' => !empty($implementation['implementation_id']) ? $implementation['implementation_id'] : 0,
  );

  $form['field_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Field ID'),
    '#required' => TRUE,
    '#default_value' => !empty($implementation['field_id']) ? $implementation['field_id'] : '#edit-search-block-form',
    '#description' => t('The id for the html selector.  eg #edit-search-block-form.'),
  );
  
  $form['tagit_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form ID'),
    '#required' => TRUE,
    '#default_value' => !empty($implementation['form_id']) ? $implementation['form_id'] : 'search_block_form',
    '#description' => t('The Drupal form id for the form where the selector is present, eg search_block_form.'),
  );
  
  $form['autocomplete_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete url'),
    '#required' => TRUE,
    '#default_value' => !empty($implementation['autocomplete_path']) ? $implementation['autocomplete_path'] : 'taxonomy/autocomplete',
    '#description' => t('The url for the automplete, eg: taxonomy/autocomplete'),
  );
  
  $form['minimum_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum string length'),
    '#size' => 8,
    '#default_value' => !empty($implementation['minimum_length']) ? $implementation['minimum_length'] : 2,
    '#description' => t('The minimum string length required before autocomplete suggestions are returned.'),
  );
 
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Provide label'),
    '#size' => 64,
    '#required' => TRUE,
    '#default_value' => !empty($implementation['label']) ? $implementation['label'] : '',
  );
  
  $form['parameter1'] = array(
    '#type' => 'textfield',
    '#title' => t('First Parameter'),
    '#default_value' => !empty($implementation['settings']['parameter1']) ? $implementation['settings']['parameter1'] : '',
    '#description' => t('If your autocomplete is altered by a field then enter the id or class in the form #idname or .classname.  Leave empty for no effect to take place.'),
  );
  
  $form['tag_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag Limit'),
    '#default_value' => !empty($implementation['settings']['tag_limit']) ? $implementation['settings']['tag_limit'] : '',
    '#description' => t('Limit the number of tags that can be entered into the field.  Leave empty for no limit.'),
  );
  
  $form['single_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Single Field'),
    '#description' => t('Check this box to make the submitted field a single field with comma separated values.'),
    '#default_value' => !empty($implementation['settings']['single_field']) ? $implementation['settings']['single_field'] : '',
  );
  
  $form['allow_spaces'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow Spaces'),
    '#description' => t('Allow spaces. If this is turned off then a space is equivalent to carriage return and ends the word.'),
    '#default_value' => !empty($implementation['settings']['allow_spaces']) ? $implementation['settings']['allow_spaces'] : '',
  );
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('tagit options'),
  );
  $form['options']['after_tag_added_function'] = array(
    '#type' => 'textfield',
    '#title' => t('Function to run after a tag is added'),
    '#default_value' => !empty($implementation['settings']['after_tag_added_function']) ? $implementation['settings']['after_tag_added_function'] : '',
    '#description' => t('The name of a function you\'d like to run after the tag is added.  You must have this function loaded on the page. If this field has a value then it takes precedence over the option that follows'),
  );

  $form['options']['after_tag_added'] = array(
    '#type' => 'textarea',
    '#title' => t('Action to take after a tag is added'),
    '#default_value' => !empty($implementation['settings']['after_tag_added']) ? $implementation['settings']['after_tag_added'] : '',
    '#description' => t('Enter with a new line for each, the jquery actions that should occur when a tag is added. If there is a function name entered for after a tag is added, then this field is ignored.'),
  );

  $form['options']['after_tag_removed_function'] = array(
    '#type' => 'textfield',
    '#title' => t('Function to run after a tag is removed'),
    '#default_value' => !empty($implementation['settings']['after_tag_removed_function']) ? $implementation['settings']['after_tag_removed_function'] : '',
    '#description' => t('The name of a function you\'d like to run after the tag is removed.  You must have this function loaded on the page. If this field has a value then it takes precedence over the option that follows'),
  );

  $form['options']['after_tag_removed'] = array(
    '#type' => 'textarea',
    '#title' => t('Action to take after a tag is removed'),
    '#default_value' => !empty($implementation['settings']['after_tag_removed']) ? $implementation['settings']['after_tag_removed'] : '',
    '#description' => t('Enter with a new line for each, the jquery actions that should occur when a tag is removed. If there is a function name entered for after a tag is removed, then this field is ignored.'),
  );

  $form['options']['on_tag_exists'] = array(
    '#type' => 'textarea',
    '#title' => t('Action to take if a tag already exists'),
    '#default_value' => !empty($implementation['settings']['on_tag_exists']) ? $implementation['settings']['on_tag_exists'] : '',
    '#description' => t('Enter with a new line for each, the jquery actions that should occur when a tag already exists.'),
  );

  $form['options']['limit_exceeded'] = array(
    '#type' => 'textarea',
    '#title' => t('Action to take if the tag limit is exceeded'),
    '#default_value' => !empty($implementation['settings']['limit_exceeded']) ? $implementation['settings']['limit_exceeded'] : '',
    '#description' => t('Enter with a new line for each, the jquery actions that should occur when the tag limit is exceeded.'),
  );

  // Button for the corresponding actions
  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#redirect' => 'admin/config/user-interface/tagit',
    '#value' => t('Save'),
  );
  $form['actions']['submit_edit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and edit'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/user-interface/tagit',
  );

  $form['#submit'][] = 'tagit_implementation_settings_form_submit';

  return $form;
}


function tagit_implementation_settings_form_validate($form, &$form_state) {
  return;
  // Performs basic validation of the menu path.
  if (url_is_external($form_state['values']['search_path'])) {
    form_set_error('search_path', t('Path must be local.'));
  }
  $form_state['values']['search_path'] = trim($form_state['values']['search_path'], '/');
  if (empty($form_state['values']['search_path'])) {
    form_set_error('search_path', t('Path required.'));
  }
  if (!is_numeric($form_state['values']['advanced']['tagit_per_page'])) {
    form_set_error('advanced][tagit_per_page', t('The amount of search results must be an integer.'));
  }
  $form_state['values']['advanced']['tagit_per_page'] = (int) $form_state['values']['advanced']['tagit_per_page'];
  if (empty($form_state['values']['advanced']['tagit_per_page'])) {
    form_set_error('advanced][tagit_per_page', t('The amount of search results cannot be empty.'));
  }
  if ($form_state['values']['page_id'] == 'core_search') {
    if (!preg_match('@^search/[^/%]+$@', $form_state['values']['search_path'])) {
      form_set_error('search_path', t('The core Search page path must start with search/ and only have one /'));
    }
  }
  elseif (count(explode('%', $form_state['values']['search_path'])) > 2) {
    form_set_error('search_path', t('Only one % placeholder is allowed.'));
  }
}

/**
 * Processes tagit_page_settings_form form submissions.
 */
function tagit_implementation_settings_form_submit($form, &$form_state) {
  tagit_implementation_save($form_state['values']);

  // Saves our values in the database, sets redirect path on success.
  drupal_set_message(t('The configuration options have been saved for %implementation.', array('%implementation' => $form_state['values']['field_id'])));
  if (isset($form_state['clicked_button']['#redirect'])) {
    $form_state['redirect'] = $form_state['clicked_button']['#redirect'];
  }
  else {
    $form_state['redirect'] = current_path();
  }
  // Regardless of the destination parameter we want to go to another page
  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();
}

function tagit_implementation_save($values) {
  $settings = array();
  $settings['parameter1'] = $values['parameter1'];
  $settings['single_field'] = $values['single_field'];
  $settings['allow_spaces'] = $values['allow_spaces'];
  $settings['tag_limit'] = $values['tag_limit'];
  $settings['minimum_length'] = $values['minimum_length'];
  $settings['after_tag_added_function'] = $values['after_tag_added_function'];
  $settings['after_tag_added'] = $values['after_tag_added'];
  $settings['after_tag_removed_function'] = $values['after_tag_removed_function'];
  $settings['after_tag_removed'] = $values['after_tag_removed'];
  $settings['on_tag_exists'] = $values['on_tag_exists'];
  $settings['limit_exceeded'] = $values['limit_exceeded'];

  $implementation = array();
  $implementation['form_id'] = $values['tagit_form_id'];
  $implementation['field_id'] = $values['field_id'];
  $implementation['label'] = $values['label'];
  $implementation['autocomplete_path'] = $values['autocomplete_path'];
  $implementation['implementation_id'] = $values['implementation_id'];
  $implementation['settings'] = $settings;
  if (!empty($implementation)) {
    if ($implementation['implementation_id'] == 0) {
      $implementation['implementation_id'] = db_next_id(db_query('SELECT MAX(implementation_id) FROM {tagit_implementations}')->fetchField());
    }
    db_merge('tagit_implementations')
      ->key(array('implementation_id' => $implementation['implementation_id']))
      ->fields(array(
        'implementation_id' => $implementation['implementation_id'],
        'form_id' => $implementation['form_id'],
        'field_id' => $implementation['field_id'],
        'label' => $implementation['label'],
        'autocomplete_path' => $implementation['autocomplete_path'],
        'settings' => serialize($implementation['settings']),
      ))
      ->execute();
  }
  tagit_set_variable();
}

function tagit_set_variable() {
  $result = db_select('tagit_implementations', 't')
    ->fields('t', array('implementation_id', 'form_id'))
    ->execute()
    ->fetchAll();
	
  foreach ($result as $implementation) {
    $implementations[$implementation->implementation_id] = $implementation->form_id;
  }
  variable_set('tagit_form_implementations', $implementations);

}

/**
 * Deletes a single search page configuration.
 */
function tagit_delete_implementation_confirm($form, &$form_state, $implementation) {

  // Sets values required for deletion.
  $form['implementation_id'] = array('#type' => 'value', '#value' => $implementation['implementation_id']);
  $form['label'] = array('#type' => 'value', '#value' => $implementation['label']);

  $verb = t('Delete');

  // Sets the message, or the title of the page.
  $message = t(
    'Are you sure you want to !verb the %label search page configuration?',
    array('%label' => $form['label']['#value'], '!verb' => strtolower($verb))
  );


  // Builds caption.
  $caption = '<p>';
  $caption .= t(
    'The %label search page configuration will be deleted.',
    array('%label' => $form['label']['#value'])
  );
  $caption .= '</p>';
  $caption .= '<p><strong>' . t('This action cannot be undone.') . '</strong></p>';

  // Finalizes and returns the confirmation form.
  $return_path = 'admin/config/user-interface/tagit';
  $button_text = $verb;
  return confirm_form($form, filter_xss($message), $return_path, filter_xss($caption), check_plain($button_text));
}

/**
 * Process content type delete confirm submissions.
 */
function tagit_delete_implementation_confirm_submit($form, &$form_state) {
  // Deletes the index configuration settings.
  // @todo Invoke a hook that allows backends and indexers to delete their stuff.
  tagit_implementation_delete($form_state['values']);
  // Sets message, logs action.
  drupal_set_message(t(
    'The %label tagit configuration has been deleted.',
    array('%label' => $form_state['values']['label'])
  ));
  watchdog('tagit', 'Deleted tagit configuration "@label".', array('@label' => $form_state['values']['label']), WATCHDOG_NOTICE);

  // Resets the variable.
  tagit_set_variable();

  // Returns back to search page list page.
  $form_state['redirect'] = 'admin/config/user-interface/tagit';
}
function tagit_implementation_delete($values) {
  db_delete('tagit_implementations')
    ->condition('implementation_id', $values['implementation_id'])
    ->execute();
    
}
/**
 * Listing of all the tagit implementations
 * @return array $build
 */
function tagit_list_implementations() {
  $build = array();
  $rows = array();
  $rows['core_search'] = array();

  // Build the sortable table header.
  $header = array(
    'label' => array('data' => t('Label'), 'field' => 's.label'),
    'form' => array('data' => t('Form'), 'field' => 's.form_id'),
    'field' => array('data' => t('Field'), 'field' => 's.field_id'),
    'operations' => array('data' => t('Operations')),
  );

  $implementations = tagit_load_all_implementations();
  foreach ($implementations as $implementation) {
    $row = array();

    $row[] = check_plain($implementation['label']);
    $row[] = check_plain($implementation['form_id']);
    $row[] = check_plain($implementation['field_id']);
 
    // Operations
    $row[] = array('data' => l(t('Edit'), 'admin/config/user-interface/tagit/' . $implementation['implementation_id'] . '/edit'));

    // Allow to revert a search page or to delete it
    $row[] = array('data' => l(t('Delete'), 'admin/config/user-interface/tagit/' . $implementation['implementation_id'] . '/delete'));
    $rows[$implementation['field_id']] = $row;
  }

  // Automatically enlarge our header with the operations size
  $header['operations']['colspan'] = count(reset($rows)) - 3;

  $build['list'] = array(
    '#prefix' => '<h3>Pages</h3>',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array_values($rows),
    '#empty' => t('No available implementations.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
    '#quantity' => 20,
    '#weight' => 10,
  );

  return $build;
}
