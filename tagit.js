(function($) {
    
  Drupal.behaviors.tagit = {
    attach: function(context) {
      for (x in Drupal.settings.tagit) {
        fieldTagit(Drupal.settings.tagit[x]);
      }
    }
  }
  function fieldTagit(settings) {
      var eventTag = $(settings.field_id);
      var spellchecker = function(parser) {
        return new $.SpellChecker(null, {
          lang: 'en_GB',
          parser: parser,
          webservice: {
            path: Drupal.settings.basePath + Drupal.settings.jquery_spellchecker.library_path + '/webservices/php/SpellChecker.php',
            driver: 'enchant'
          }
        });
      };
      
      var filterSpelling = function(text) {
        /*
        spellchecker('text').check(text, function(incorrectWords) {
          console.log(incorrectWords);
        });             
        */
      };
      
      eventTag.tagit({
        autocomplete: { 
          minLength: settings.minimum_length,
          source: function( request, response ) {      
            var filter = request.term.toLowerCase();
            $.ajax({
              type: "GET",
              //  We use the standard Drupal url form
              url: Drupal.settings.basePath + settings.autocomplete_path + '/' + request.term,
              dataType: "json",
              data: {
                parameter1: $(settings.parameter1).val(),
              },
              success: function(data){
                response(  $.grep(data, function(element) {
                  // Only match autocomplete options that begin with the search term.
                  // (Case insensitive.)
                  return (element.toLowerCase().indexOf(filter) === 0);
                }));
              }
            });
          }
        },
        allowSpaces: settings.allow_spaces,
        //showAutocompleteOnFocus:true,
        singleField: settings.single_field,
        tagLimit: settings.tag_limit,
        
        beforeTagAdded: function(event, ui) {
          if (!ui.duringInitialization) {
             filterSpelling('beforeTagAdded: ' + eventTag.tagit('tagLabel', ui.tag));
          }
        },

        afterTagAdded: function(event, ui) {
          //alert(settings.after_tag_removed_function);
          if (settings.after_tag_added_function !== null) {
             var fn = settings.after_tag_added_function + '()'; 
             eval(fn);
          }
          else {
            var output = settings.after_tag_added;
            if (output != '') eval(output);
          }
        },

        afterTagRemoved: function(event, ui) {
          if (settings.after_tag_removed_function !== null) {
             var fn = settings.after_tag_removed_function + '()'; 
             eval(fn);
          }
          else {
            var output = settings.after_tag_removed;
            if (output != '') eval(output);
          }
        },
        
        onTagExists: function(event, ui) {
          var output = settings.on_tag_exists;
          if (output != '') eval(output);
        },
        
        onTagLimitExceeded: function(event, ui) {
          var output = settings.limit_exceeded;
          if (output != '') eval(output);
        }
      });
  }
  
})(jQuery);